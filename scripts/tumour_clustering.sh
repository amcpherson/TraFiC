#!/usr/bin/bash

sample=$1
tumour=$2
script_dir=$3
pathdir=$4

#clustering;

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_Alu_mas.ok.txt > $pathdir/$sample/$tumour/clusters_Alu_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_Alu_mas.ok.txt > $pathdir/$sample/$tumour/clusters_Alu_mas.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_L1_mas.ok.txt > $pathdir/$sample/$tumour/preclusters_L1_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_L1_mas.ok.txt > $pathdir/$sample/$tumour/preclusters_L1_mas.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_SVA_mas.ok.txt > $pathdir/$sample/$tumour/clusters_SVA_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_SVA_mas.ok.txt > $pathdir/$sample/$tumour/clusters_SVA_mas.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_ERVK_mas.ok.txt > $pathdir/$sample/$tumour/clusters_ERVK_mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_ERVK_mas.ok.txt > $pathdir/$sample/$tumour/clusters_ERVK_mas.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_mas.ok.PolyA.txt > $pathdir/$sample/$tumour/clusters.PolyA.mas.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_mas.ok.PolyA.txt > $pathdir/$sample/$tumour/clusters.PolyA.mas.txt


#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_Alu_menos.ok.txt > $pathdir/$sample/$tumour/clusters_Alu_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_Alu_menos.ok.txt > $pathdir/$sample/$tumour/clusters_Alu_menos.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_L1_menos.ok.txt > $pathdir/$sample/$tumour/preclusters_L1_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_L1_menos.ok.txt > $pathdir/$sample/$tumour/preclusters_L1_menos.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_SVA_menos.ok.txt > $pathdir/$sample/$tumour/clusters_SVA_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_SVA_menos.ok.txt > $pathdir/$sample/$tumour/clusters_SVA_menos.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_ERVK_menos.ok.txt > $pathdir/$sample/$tumour/clusters_ERVK_menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_ERVK_menos.ok.txt > $pathdir/$sample/$tumour/clusters_ERVK_menos.txt

#echo perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_menos.ok.PolyA.txt > $pathdir/$sample/$tumour/clusters.PolyA.menos.txt
perl $script_dir/scripts/parser3_TE.pl $pathdir/$sample/$tumour/MASKEDreads_menos.ok.PolyA.txt > $pathdir/$sample/$tumour/clusters.PolyA.menos.txt

#rm $pathdir/$sample/$tumour/MASKEDreads_mas.ok.PolyA.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_menos.ok.PolyA.txt

#rm $pathdir/$sample/$tumour/MASKEDreads_Alu_mas.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_L1_mas.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_SVA_mas.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_ERVK_mas.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_Alu_menos.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_L1_menos.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_SVA_menos.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_ERVK_menos.ok.txt

#echo perl $script_dir/scripts/cleaner_v5_forL1andPolyA.pl $pathdir/$sample/$tumour/preclusters_L1_mas.txt $pathdir/$sample/$tumour/clusters.PolyA.mas.txt > $pathdir/$sample/$tumour/clusters_L1_mas.txt
perl $script_dir/scripts/cleaner_v5_forL1andPolyA.pl $pathdir/$sample/$tumour/preclusters_L1_mas.txt $pathdir/$sample/$tumour/clusters.PolyA.mas.txt > $pathdir/$sample/$tumour/clusters_L1_mas.txt

#echo perl $script_dir/scripts/cleaner_v5_forL1andPolyA.pl $pathdir/$sample/$tumour/preclusters_L1_menos.txt $pathdir/$sample/$tumour/clusters.PolyA.menos.txt > $pathdir/$sample/$tumour/clusters_L1_menos.txt
perl $script_dir/scripts/cleaner_v5_forL1andPolyA.pl $pathdir/$sample/$tumour/preclusters_L1_menos.txt $pathdir/$sample/$tumour/clusters.PolyA.menos.txt > $pathdir/$sample/$tumour/clusters_L1_menos.txt

#se suman los clusters;

#echo cat $pathdir/$sample/$tumour/clusters_*_mas.txt > $pathdir/$sample/$tumour/clusters_mas.txt
cat $pathdir/$sample/$tumour/clusters_*_mas.txt > $pathdir/$sample/$tumour/clusters_mas.txt

#echo cat $pathdir/$sample/$tumour/clusters_*_menos.txt > $pathdir/$sample/$tumour/clusters_menos.txt
cat $pathdir/$sample/$tumour/clusters_*_menos.txt > $pathdir/$sample/$tumour/clusters_menos.txt

#echo cat $pathdir/$sample/$tumour/clusters.PolyA.mas.txt $pathdir/$sample/$tumour/clusters.PolyA.menos.txt > $pathdir/$sample/$tumour/clusters.PolyA.masymenos.txt
cat $pathdir/$sample/$tumour/clusters.PolyA.mas.txt $pathdir/$sample/$tumour/clusters.PolyA.menos.txt > $pathdir/$sample/$tumour/clusters.PolyA.masymenos.txt

#rm $pathdir/$sample/$tumour/clusters_Alu_mas.txt
#rm $pathdir/$sample/$tumour/preclusters_L1_mas.txt
#rm $pathdir/$sample/$tumour/clusters_SVA_mas.txt
#rm $pathdir/$sample/$tumour/clusters_ERVK_mas.txt
#rm $pathdir/$sample/$tumour/clusters_L1_mas.txt
#rm $pathdir/$sample/$tumour/clusters_Alu_menos.txt
#rm $pathdir/$sample/$tumour/preclusters_L1_menos.txt
#rm $pathdir/$sample/$tumour/clusters_SVA_menos.txt
#rm $pathdir/$sample/$tumour/clusters_ERVK_menos.txt
#rm $pathdir/$sample/$tumour/clusters_L1_menos.txt
