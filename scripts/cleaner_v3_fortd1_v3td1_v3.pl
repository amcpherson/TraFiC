#from /nfs/users/nfs_j/jt14/Desktop/cleaner_v3_fortd1_v3.pl but adapted to td1;
#this version includes the analysis of the master hg19 copies;
#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use Utils;

if (scalar(@ARGV) != 6){
        print "Incorrect number of inputs";
        exit -1;
}

my $infile1 = $ARGV[0];#archivo con los filter3.td2.4C.coordinates.txt;
my $infile2 = $ARGV[1];#archivo con la base de master copies;
my $infile3 = $ARGV[2];#archivo con el conjunto de MGE4C y tdmasmenos del tumor y del normal;
my $infile4 = $ARGV[3];#archivo con las master copies del genoma de referencia;
my $word = $ARGV[4];
my $pathdir = $ARGV[5];

open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");#archivo con los filter3.td2.4C.coordinates.txt;
my @file1_lines = <FILE1>;
chomp(@file1_lines);
close(FILE1);


open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");#archivo con la base de master copies;
my @file2_lines = <FILE2>;
chomp(@file2_lines);
close(FILE2);


open(FILE3, "<$infile3") or die("Couldn't open $infile3: $!\n");#archivo con los clusters mas y menos del tumour;
my @file3_lines = <FILE3>;
chomp(@file3_lines);
close(FILE3);

open(FILE4, "<$infile4") or die("Couldn't open $infile4: $!\n");#archivo con las master copies del genoma de referencia;
my @file4_lines = <FILE4>;
chomp(@file4_lines);
close(FILE4);


my $chr2data2 = slice_by_chr(\@file2_lines);
my $chr2data3 = slice_by_chr(\@file3_lines);
my $chr2data4 = slice_by_chr(\@file4_lines);


my $outfile1 = "$pathdir/finaltd1.master.$word.txt";
my $outfile2 = "$pathdir/finaltd1.putative.$word.txt";
my $outfile3 = "$pathdir/finaltd1.master_putative.$word.txt";

open(OUT1, ">$outfile1") or die("Couldn't open $outfile1\n");
open(OUT2, ">$outfile2") or die("Couldn't open $outfile2\n");
open(OUT3, ">$outfile3") or die("Couldn't open $outfile3\n");


foreach my $line1 (@file1_lines){
        my $flag='';
        
	my ($T_chr,$T_pos1L,$T_pos1U,$support_pos1,$TEfam,$readname_pos1,undef,$T_pos2L,$T_pos2U,$support_pos2,undef,$readnamepos2,$gene,$chrA,$posA) = split('\t',$line1);
	#print $T_chr,undef,$T_pos1,undef,undef,undef,undef,$T_pos2"
       
	
	if (exists $chr2data2->{$chrA})
	{
		foreach my $line2 (@{$chr2data2->{$chrA}}){
			my ($master_chr,$master_pos1,$master_pos2,$master_strand) = split('\t',$line2);

			if ($master_chr ne $T_chr){

				if (($chrA eq $master_chr) && (((abs($master_pos1 - $posA)) <= 20000) || ((abs($master_pos2 - $posA)) <= 20000))){
		       	
					print(OUT1  "$line1\t$line2\n");
				} 
			}else{
				
				if (((abs($master_pos1 - $T_pos1U) > 500000)) && ($chrA eq $master_chr) && (((abs($master_pos1 - $posA)) <= 20000) || ((abs($master_pos2 - $posA)) <= 20000))){
	
				print(OUT1  "$line1\t$line2\n");
				}
			}
		}
	}
	
	if (exists $chr2data3->{$chrA})
	{
		foreach my $line3 (@{$chr2data3->{$chrA}}){
			my ($chr_cluster,$pos1_cluster,$pos2_cluster,$supp_cluster,$TEfam_cluster,$readname_cluster,$chr_cluster_neg,$pos1_cluster_neg,$pos2_cluster_neg,$supp_cluster_neg,$TEfam_cluster_neg,$readname_cluster_neg) = split('\t',$line3);

			if ($chr_cluster ne $T_chr){

				if (($TEfam_cluster =~ "L1") && ($chrA eq $chr_cluster) && ((abs($pos2_cluster - $posA)) <= 20000)){
	
					print(OUT2 "$line1\t$chr_cluster\t$pos1_cluster\t$pos2_cluster\tputative\n");
				}
			}else{

				if ((abs($pos1_cluster - $T_pos1U) > 500000) && ($TEfam_cluster =~ "L1") && ($chrA eq $chr_cluster) && ((abs($pos2_cluster - $posA)) <= 20000)){

					print(OUT2 "$line1\t$chr_cluster\t$pos1_cluster\t$pos2_cluster\tputative\n");
				}
			}
		}
	}
	
	if (exists $chr2data4->{$chrA})
	{
		foreach my $line4 (@{$chr2data4->{$chrA}}){
			my ($chr_master_putative,$pos1_master_putative,$pos2_master_putative,$strand_master_putative,$div,$subfam,$fam) = split('\t',$line4);
	
			if ($chr_master_putative ne $T_chr){
	
				if (($fam =~ "L1") && ($chrA eq $chr_master_putative) && (((abs($pos1_master_putative - $posA)) <= 20000) || ((abs($pos2_master_putative - $posA)) <= 20000))){
	
					print(OUT3 "$line1\t$chr_master_putative\t$pos1_master_putative\t$pos2_master_putative\t$strand_master_putative\n");
	
				}
			}else{
	
				if ((abs($pos1_master_putative - $T_pos1U) > 500000) && ($fam =~ "L1") && ($chrA eq $chr_master_putative) && (((abs($pos1_master_putative - $posA)) <= 20000) || ((abs($pos2_master_putative - $posA)) <= 20000))){
	
					print(OUT3 "$line1\t$chr_master_putative\t$pos1_master_putative\t$pos2_master_putative\t$strand_master_putative\n");	
				}

			}
		}
	
	}
	
}

close(OUT1);
close(OUT2);
close(OUT3);
