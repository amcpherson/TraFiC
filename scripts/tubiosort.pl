 use File::Slurp;

  my @lines = read_file( $ARGV[0] ) ;

  my @AofA; 
  foreach(@lines)
  {
    chomp $_;
    push (@AofA, [split("\t",$_)]);
  }
 @AofA =  sort { $a->[6] cmp $b->[6] || $a->[1] <=> $b->[1] ||  $a->[2] <=> $b->[2]  } @AofA;
  
 foreach(@AofA){ print join("\t",@$_), "\n"; }