#this script identify the coordinates of a transduced region in the donor chromosome;
#this script is the same as /nfs/team78pc8/jt14/TEs/td_coordinates_v2.pl, but for td1 (prints all chromosomes allowing the detection of TE patterns);
#!/usr/bin/perl -w

use strict;

if (scalar(@ARGV) != 3){
        print "Incorrect number of files\n";
}

my $infile1 = $ARGV[0];
my $infile2 = $ARGV[1];
my $pathdir = $ARGV[2];

my $outfile1 = "$pathdir/filter3.td.4C.coordinates.mas.txt";
my $outfile2 = "$pathdir/filter3.td.4C.coordinates.menos.txt";

open(OUT1, ">$outfile1") or die("Couldn't open $outfile1\n");
open(OUT2, ">$outfile2") or die("Couldn't open $outfile2\n");


open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");

while (my $line1 = <FILE1>){
	$line1 =~ s/\n//g;
	my @aline1 = split('\t', $line1);
#	print ("@aline1\n");
	my @reads_pos = split(',', $aline1[5]);
#	print ("@reads_pos\n");
	my @reads_neg = split(',', $aline1[11]);

	my @positions = ();
	my @chromosomes = ();


	foreach (@reads_pos){

		open(FILE2, "<$infile2") or die ("Couldn't open $infile2: $!\n");

		while (my $line2 = <FILE2>){
			$line2 =~ s/\n//g;
			my @aline2 = split('\t', $line2);
			my $read_name = $aline2[0];
			my $chr1 = $aline2[2];
			my $pos1 = $aline2[3];
			my $chr2 = $aline2[6];
			my $pos2 = $aline2[7];

			if (($_ eq $read_name) && ($chr1 eq $aline1[0]) && (abs($pos1 - $aline1[2]) < 500)){
				#push (@positions, $pos2);
				if ($chr2 eq "="){
					push (@chromosomes, $chr1);
					print (OUT1 "$line1\t$chr1\t$pos2\n");
				}else{
					push (@chromosomes, $chr2);
					print (OUT1 "$line1\t$chr2\t$pos2\n");
				}
#			print("$_\t$read_name\t$chr1\t$aline1[0]\t$pos1\t$aline1[2]\t$aline2[6]\t$aline2[7]\n");
			
			}
			if (($_ eq $read_name) && ($chr2 eq $aline1[0]) && (abs($pos2 - $aline1[2]) < 500)){
				push (@positions, $pos1);
				push (@chromosomes, $chr1);
				print (OUT1 "$line1\t$chr1\t$pos1\n");
				
			}
			if (($_ eq $read_name) && ($chr2 eq "=")){
				if (($chr1 eq $aline1[0]) && (abs($pos2 - $aline1[2]) < 500 )){
					push (@positions, $pos1);
					push (@chromosomes, $chr1);
					print (OUT1 "$line1\t$chr1\t$pos1\n");

				}

			}
		}
	}
	my @sorted_pos = sort {$a <=> $b} @positions;
	my @sorted_chr = sort {$a <=> $b} @chromosomes;

	@sorted_pos = ();
	@sorted_chr = ();
	close(FILE2);
	#---------------------------------

	my @positions_neg = ();
	my @chromosomes_neg = ();

        foreach (@reads_neg){
		
		open(FILE2, "<$infile2") or die ("Couldn't open $infile2: $!\n");

		while (my $line2_neg = <FILE2>){
			$line2_neg =~ s/\n//g;
			my @aline2_neg = split('\t', $line2_neg);
                        my $read_name_neg = $aline2_neg[0];
                        my $chr1_neg = $aline2_neg[2];
                        my $pos1_neg = $aline2_neg[3];
                        my $chr2_neg = $aline2_neg[6];
                        my $pos2_neg = $aline2_neg[7];
			
			if (($_ eq $read_name_neg) && ($chr1_neg eq $aline1[0]) && (abs($pos1_neg - $aline1[2]) < 500)){

				if ($chr2_neg eq "="){
					push (@chromosomes_neg, $chr1_neg);
					print (OUT2 "$line1\t$chr1_neg\t$pos2_neg\n");
				}else{
					push (@chromosomes_neg, $chr2_neg);
					print (OUT2 "$line1\t$chr2_neg\t$pos2_neg\n");
				}

			}
			if (($_ eq $read_name_neg) && ($chr2_neg eq $aline1[0]) && (abs($pos2_neg - $aline1[2]) < 500)){
				push (@positions_neg, $pos1_neg);
				push (@chromosomes_neg, $chr1_neg);
				print (OUT2 "$line1\t$chr1_neg\t$pos1_neg\n");

			}
			if (($_ eq $read_name_neg) && ($chr2_neg eq "=")){
				if (($chr1_neg eq $aline1[0]) && (abs($pos2_neg - $aline1[2]) < 500 )){
					push (@positions_neg, $pos1_neg);
					push (@chromosomes_neg, $chr1_neg);
					print (OUT2 "$line1\t$chr1_neg\t$pos1_neg\n");

				}

			}
		}
	}
	my @sorted_pos_neg = sort {$a <=> $b} @positions_neg;
	my @sorted_chr_neg = sort {$a <=> $b} @chromosomes_neg;

	@sorted_pos_neg = ();
	@sorted_chr_neg = ();
	close(FILE2);

}

close(OUT1);
close(OUT2);
