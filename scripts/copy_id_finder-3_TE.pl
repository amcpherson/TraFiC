#!/usr/bin/perl -w


use strict;

if (scalar(@ARGV) != 2){
	print "Incorrect number of files\n";
}

my $infile1 = $ARGV[0];
my $infile2 = $ARGV[1];

my %hash = ();


open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");

while (my $line1 = <FILE1>){
	$line1 =~ s/\n//g;
	my @aline1 = split('\t', $line1);
	my $file1_readname = $aline1[1];
#	print "$file1_readname-";
	$hash{$file1_readname} = $line1;
}


open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");

	while (my $line2 = <FILE2>){ #reading file2 line by line
		chomp $line2; #removing enters
		my @aline2 = split('\t', $line2); #fields of each line are stored in an array
		my $file2_readname = $aline2[0]; #this variable contains the readname of each line in file2
	#	print "$file2_readname-";
		if ($hash{$file2_readname}){
			print("$hash{$file2_readname}\t$line2\n");
		}
	}
close(FILE1);
close(FILE2);
