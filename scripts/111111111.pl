#!/usr/bin/perl -w

use strict;

if (scalar(@ARGV) != 2){
        print "Incorrect number of files\n";
}

my $pathdir = $ARGV[0];
my $sample = $ARGV[1];

`cat $pathdir/deftd1.master.ok.txt $pathdir/deftd1.putative.ok.txt $pathdir/deftd1.master_putative.ok.txt > $pathdir/deftd1.all.txt`; 
`cat $pathdir/deftd2.master.ok.txt $pathdir/deftd2.putative.ok.txt $pathdir/deftd2.master_putative.ok.txt > $pathdir/deftd2.all.txt`;

my $infile1 = "$pathdir/filter3.td0.4C.txt";
my $infile2 = "$pathdir/deftd1.all.txt";
my $infile3 = "$pathdir/deftd2.all.txt";

#open(OUT1, ">$outfile1") or die("Couldn't open $outfile1\n");


open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");
open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");
open(FILE3, "<$infile3") or die("Couldn't open $infile3: $!\n");
#open(FILE4, "<$infile4") or die("Couldn't open $infile4: $!\n");
#open(FILE5, "<$infile5") or die("Couldn't open $infile5: $!\n");
#open(FILE6, "<$infile6") or die("Couldn't open $infile6: $!\n");
#open(FILE7, "<$infile7") or die("Couldn't open $infile7: $!\n");


while (my $line1 = <FILE1>){
	$line1 =~ s/\n//g;
	my @aline1 = split('\t', $line1);
	my $new_pos = $aline1[2]+100;

	if (($aline1[0] ne "MT") && ($aline1[0] ne "Y")){
		
		print ("$sample\t$aline1[0]\t$new_pos\t$aline1[7]\t$aline1[4]\t$aline1[12]\ttd0\t-\t-\t-\t-\t-\t-\t-\t-\n");
		#print ("$sample\t$aline1[0]\t$new_pos\t$aline1[7]\t$aline1[4]\t$aline1[12]\tsolo\t-\t-\t-\t-\t-\t-\t-\t-\n");
	}
}

while (my $line2 = <FILE2>){
	$line2 =~ s/\n//g;
	my @aline2 = split('\t', $line2);
	my $new_pos = $aline2[1]+100;
	my $dif_plus = $aline2[10] - $aline2[6];
	my $dif_minus = $aline2[5] - $aline2[9];

	if ($aline2[7] eq "plus"){
		#print ("$sample\t$aline2[0]\t$new_pos\t$aline2[2]\tL1\t$aline2[3]\tpartnered\t$aline2[4]\t$aline2[5]\t$aline2[6]\t$aline2[7]\t-\t$aline2[10]\t$dif_plus\t-\n");
		print ("$sample\t$aline2[0]\t$new_pos\t$aline2[2]\tL1\t$aline2[3]\ttd1\t$aline2[4]\t$aline2[5]\t$aline2[6]\t$aline2[7]\t-\t$aline2[10]\t$dif_plus\t-\n");
	}
	if ($aline2[7] eq "minus"){
		#print ("$sample\t$aline2[0]\t$new_pos\t$aline2[2]\tL1\t$aline2[3]\tpartnered\t$aline2[4]\t$aline2[5]\t$aline2[6]\t$aline2[7]\t$aline2[9]\t-\t$dif_minus\t-\n");
		print ("$sample\t$aline2[0]\t$new_pos\t$aline2[2]\tL1\t$aline2[3]\ttd1\t$aline2[4]\t$aline2[5]\t$aline2[6]\t$aline2[7]\t$aline2[9]\t-\t$dif_minus\t-\n");
	}	
	if ($aline2[7] eq "putative"){
		 #print ("$sample\t$aline2[0]\t$new_pos\t$aline2[2]\tL1\t$aline2[3]\tpartnered\t$aline2[4]\t$aline2[5]\t$aline2[6]\t$aline2[7]\t$aline2[9]\t$aline2[10]\t-\t-\n");
		 print ("$sample\t$aline2[0]\t$new_pos\t$aline2[2]\tL1\t$aline2[3]\ttd1\t$aline2[4]\t$aline2[5]\t$aline2[6]\t$aline2[7]\t$aline2[9]\t$aline2[10]\t-\t-\n");
}

}

while (my $line3 = <FILE3>){
	$line3 =~ s/\n//g;
	my @aline3 = split('\t', $line3);
	my $new_pos = $aline3[1]+100;
	my $dif_plus = $aline3[10] - $aline3[6];
	my $dif_minus = $aline3[5] - $aline3[9];
	my $transduction_size = $aline3[10] - $aline3[9];

	if ($aline3[7] eq "plus"){
		#print ("$sample\t$aline3[0]\t$new_pos\t$aline3[2]\tL1\t$aline3[3]\torphan\t$aline3[4]\t$aline3[5]\t$aline3[6]\t$aline3[7]\t$aline3[9]\t$aline3[10]\t$dif_plus\t$transduction_size\n");
		print ("$sample\t$aline3[0]\t$new_pos\t$aline3[2]\tL1\t$aline3[3]\ttd2\t$aline3[4]\t$aline3[5]\t$aline3[6]\t$aline3[7]\t$aline3[9]\t$aline3[10]\t$dif_plus\t$transduction_size\n");
	}
	if ($aline3[7] eq "minus"){
		#print ("$sample\t$aline3[0]\t$new_pos\t$aline3[2]\tL1\t$aline3[3]\torphan\t$aline3[4]\t$aline3[5]\t$aline3[6]\t$aline3[7]\t$aline3[9]\t$aline3[10]\t$dif_minus\t$transduction_size\n");
		print ("$sample\t$aline3[0]\t$new_pos\t$aline3[2]\tL1\t$aline3[3]\ttd2\t$aline3[4]\t$aline3[5]\t$aline3[6]\t$aline3[7]\t$aline3[9]\t$aline3[10]\t$dif_minus\t$transduction_size\n");
	}
	if ($aline3[7] eq "putative"){
		#print ("$sample\t$aline3[0]\t$new_pos\t$aline3[2]\tL1\t$aline3[3]\torphan\t$aline3[4]\t$aline3[5]\t$aline3[6]\t$aline3[7]\t$aline3[9]\t$aline3[10]\t-\t$transduction_size\n");
		print ("$sample\t$aline3[0]\t$new_pos\t$aline3[2]\tL1\t$aline3[3]\ttd2\t$aline3[4]\t$aline3[5]\t$aline3[6]\t$aline3[7]\t$aline3[9]\t$aline3[10]\t-\t$transduction_size\n");
	}
}

