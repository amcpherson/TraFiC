#!/usr/bin/perl -w

use strict;

#este program genera una sola linea a partir de una linea y la anterior si el nombre de los reads es identico;

if (scalar(@ARGV)!=1){
	print "ERROR: there must be one input file\n";
	exit -1
}

my $infile = $ARGV[0];

open (FILE,"<$infile");

my $line = "";
my $preline = "";
my $nline = 0;

my @chromosomes = ();
my @positions = ();
my @contador = ();

my $comodin1 = ();

	while ($line = <FILE>) {
		chomp $line;
	
		if ($nline > 0) {
#			@tmp = ();
#			$tmp[0] = $preline;
			my @tmp0 = split('\t',$preline);
#			$tmp[1] = $line;
			my @tmp1 = split('\t',$line);

			if (($tmp0[0] eq $tmp1[0]) && ($tmp0[1] == $tmp1[1]) && ($tmp0[13] eq $tmp1[13]) && (abs($tmp0[14] - $tmp1[14]) < 5000)){

				if ((scalar @chromosomes == 0) && (scalar @positions == 0)){

					push (@chromosomes, $tmp0[13]);
					push (@chromosomes, $tmp1[13]);
					push (@positions, $tmp0[14]);
					push (@positions, $tmp1[14]);
					if (abs($tmp1[14] - $tmp0[14]) > 0){
						push (@contador, $tmp0[14]);
						push (@contador, $tmp1[14]);
					}else{
						push (@contador, $tmp1[14]);
					}

				}else{

					push (@chromosomes, $tmp1[13]);
					push (@positions, $tmp1[14]);
					if (abs($tmp1[14] - $tmp0[14]) > 1){
#						print ("$tmp1[14],$tmp0[14]\n");
						push (@contador, $tmp1[14]);

					}


				}
				$comodin1 = ("$tmp0[0]\t$tmp0[2]\t$tmp0[7]\t$tmp0[12]\t$tmp0[15]\t$tmp0[16]\t$tmp0[17]\t$tmp0[18]");

#				print("PRELINE:@tmp0\nLINE:@tmp1\n------------------\n");
			}else{
				if ((scalar @positions > 0)){
#					my @sorted_chr = sort {$a <=> $b} @chromosomes;
					my @sorted_pos = sort {$a <=> $b} @positions;
					my @sorted_con = sort {$a <=> $b} @contador;
					my $length_contador = scalar (@contador);
					my $last_read = $sorted_pos[-1] + 100;

						print ("$tmp0[0]\t$tmp0[2]\t$tmp0[7]\t$tmp0[12]\t$tmp0[15]\t$tmp0[16]\t$tmp0[17]\t$tmp0[18]\t$chromosomes[0]\t$sorted_pos[0]\t$last_read\t$length_contador\n");
						@chromosomes = ();
						@positions = ();
						@contador = ();

				}

			}
			
		}
		
		$nline ++;
		$preline = $line;
		if (eof and  (scalar(@positions) > 0))
		{
			my @sorted_pos = sort {$a <=> $b} @positions;
			my @sorted_con = sort {$a <=> $b} @contador;
			my $length_contador = scalar (@contador);
			warn("SORTED_POSITIONS");
			warn("@sorted_pos");
			warn("______________");
			
			my $last_read = $sorted_pos[-1] + 100;

			print "$comodin1\t$chromosomes[0]\t$sorted_pos[0]\t$last_read\t$length_contador\n"
		};

	}
close FILE;

	
