#!/usr/bin/bash

sample=$1
tumour=$2
script_dir=$3
pathdir=$4

#TRANSDUCTIONS

awk 'OFS="\t" {if (($1 == $11) && (($3 != $13) || (($3 == $13) && ((($14 - $4) > 10000) || (($14 - $4) < -10000))))) {print$1,$2,$3,$4,$5,$6,$13,$14,$15,$10"\n"$11,$12,$13,$14,$15,$16,$3,$4,$5,$20}}' $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.sam > $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.sam

sort -k 3,3 -k 4,4n $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.sam > $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.ok.sam

#rm $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.sam
#rm $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.sam

awk 'OFS="\t" {if (((($3 != $7) || (($3 == $7) && ((($8 - $4) > 10000) || (($8 - $4) < -10000))))) && (($2 == 161) || ($2 == 97) || ($2 == 65) || ($2 == 129))){print $1,$3,$4,$7,$8,"+"}}' $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.ok.sam > $pathdir/$sample/$tumour/$tumour.chrpos.td.sam

awk 'OFS="\t" {if (((($3 != $7) || (($3 == $7) && ((($8 - $4) > 10000) || (($8 - $4) < -10000)))))  && (($2 == 81) || ($2 == 145) || ($2 == 113) || ($2 == 177))){print $1,$3,$4,$7,$8,"-"}}' $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.ok.sam > $pathdir/$sample/$tumour/$tumour.chrneg.td.sam

#rm $pathdir/$sample/$tumour/$tumour.dis.abr.ok.1l.td.ok.sam
