#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use Utils;

#hace una limpieza eliminando aquellas predicciones sobre inserciones de elementos moviles que, por la posicion que ocupan, pudieran tratarse de falsos 

if (scalar(@ARGV) != 4){
	print "Incorrect number of inputs";
}

my $infile1 = $ARGV[0];#contiene las predicciones de los TEs;
my $infile2 = $ARGV[1];#contiene la base de datos hg19 de TEs;
my $read_size = $ARGV[2];#size del read de la libreria;
my $divergencia = $ARGV[3];#divergencia que se quiere limitar. Ejemplo: si se quiere limitar al 2% debe ponerse un 20, si se quiere a un 10% debe ponerse un 100;



open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");
my @file1_lines = <FILE1>;
#@file1_lines = chomp(@file1_lines);
close(FILE1);

open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");
my @file2_lines = <FILE2>;
#@file2_lines = chomp(@file2_lines);
close(FILE2);


my $chr2data = slice_by_chr(\@file2_lines);

foreach my $line1 (@file1_lines){
	#print $line1;
	chomp $line1;
	my ($chr_plus,undef,$pos2_plus,undef,$TE_type,undef,$chr_minus,$pos1_minus,undef,undef,undef,undef) = split('\t',$line1);
	my $final_pos2_plus = $pos2_plus + $read_size;
	my $counter = 0;
	
	if (not exists $chr2data->{$chr_minus})
	{
		print "$line1\n";
		next;
	}
	
	my @chr_data = @{$chr2data->{$chr_minus}};	
    foreach my $line2 (@chr_data)
    {
		chomp $line2;
		my ($hg19chr,$hg19pos1,$hg19pos2,$div,undef,undef,$TEfam) = split('\t',$line2);

		unless (($hg19chr eq $chr_minus) && ((($final_pos2_plus < $hg19pos2) && ($final_pos2_plus > $hg19pos1)) || (($pos1_minus < $hg19pos2) && ($pos1_minus > $hg19pos1))) && ($TEfam eq $TE_type) && ($div <= $divergencia))
		{
			0;
		}
		else
		{
			$counter ++;
			#print "$line1\t$line2\n";
        }
	}

	#	print "$counter\n";
	if ($counter == 0){print "$line1\n"}
}

