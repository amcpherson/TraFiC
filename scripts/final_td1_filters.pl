#This script compares the files deftd2.ok.mas y deftd2.ok.menos and filter false td2 predictions;

#!/usr/bin/perl -w
use strict;

if (scalar(@ARGV) != 1){
	print "Incorrect number of inputs";
}

my $infile1 = $ARGV[0];#contiene la suma de los deftd1.ok.mas;

open(FILE1, "<$infile1") or die ("Couldn't open $infile1: $!\n");
my @file1_lines = <FILE1>;
close(FILE1);

foreach my $line1 (@file1_lines){
	chomp $line1;
	my ($chr_hit_pos,$pos1_hit_pos,$pos2_hit_pos,$gene,$chr_master_pos,$pos1_master_pos,$pos2_master_pos,$master_condition,$chr_cluster_pos,$pos1_cluster_pos,$pos2_cluster_pos,$reads_pos) = split('\t',$line1);

		if (($chr_hit_pos ne "MT") && ($chr_hit_pos ne "Y") && (($reads_pos) > 3) && (abs($pos1_hit_pos - $pos1_cluster_pos) > 1000) && (($chr_hit_pos ne $chr_master_pos) || (($chr_hit_pos eq $chr_master_pos) && ((abs($pos1_hit_pos - $pos1_cluster_pos) > 20000) && (abs($pos2_hit_pos - $pos2_cluster_pos) > 20000))))){
		
			if (($master_condition eq "plus") && ($pos2_cluster_pos > $pos1_master_pos)){

			print "$line1\n";

			}

			if (($master_condition eq "minus") && ($pos2_master_pos > $pos1_cluster_pos)){

			print "$line1\n";

			}

			if ($master_condition eq "putative"){

			print "$line1\n";

			}

		}
}

close(FILE1);
