#Se trata de parser3_TE.pl adaptado para TraFiC_transduciton.sh;

use strict;

my $preline;
my $current;
my $prevline = 0;

my $incluster = 1; 
my @cluster;


while(<>){
    chomp $_;

    if($. - $prevline == 1){ 
       $preline = $current;
    }
    $current = $_;
    $prevline++;

    if($. == 1){next;}     
    
    if (juntar($preline,$current)){ 
      push(@cluster,($preline,$current));
    }
    
    elsif(not juntar($preline,$current) ){
        my @final = finalize(@cluster);
        print (join("\t",@final),"\n") if scalar @cluster != 0;
        @cluster=();
       
    }
	if (eof) {
	     my @final = finalize(@cluster);
             print (join("\t",@final),"\n") if scalar @cluster != 0;
        }    
    
}


sub juntar{
   my ($preline,$current) = @_;
   my($pread,$pchr,$ppos,$pchr_mate,$ppos_mate,$pstrand) = split("\t",$preline);
   my($cread,$cchr,$cpos,$cchr_mate,$cpos_mate,$cstrand) = split("\t",$current);
   

   if( ($pchr eq $cchr) && (($cpos - $ppos) <=200) && ($pchr_mate eq $cchr_mate) && ((abs($cpos_mate - $ppos_mate)) <= 200) ){
       return(1);
   } 
   else{
     return(0);
   }
}

sub finalize{
   my @cluster = @_;
   my $first = $cluster[0];
   my $last = $cluster[scalar @cluster - 1];
   my ($fread,$fchr,$fpos,$fchr_mate,$fpos_mate,$fstrand) = split("\t",$first);
   my ($lread,$lchr,$lpos,$lchr_mate,$lpos_mate,$lstrand) = split("\t",$last);
   
   my %unique;
   my @reads; 
   foreach(@cluster){
       my ($read) = split("\t",$_);
       $unique{$read}++; 
   }
   
   my $reads = join(',',keys %unique);
   return($fchr,$fpos,$lpos,scalar keys %unique,"L1",$reads); 
}

