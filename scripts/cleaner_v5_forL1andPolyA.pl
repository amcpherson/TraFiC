#!/usr/bin/perl -w
use strict;

#Variante de cleaner_v5.pl para sumar clusters de L1 y PolyA;

if (scalar(@ARGV) != 2){
        print "Incorrect number of inputs";
        exit -1;
}

my $infile1 = $ARGV[0];#clusters L1;
my $infile2 = $ARGV[1];#clusters PolyA;


open(FILE1, "<$infile1") or die("Couldn't open $infile1: $!\n");
my @file1_lines = <FILE1>;
chomp(@file1_lines);
close(FILE1);


open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");
my @file2_lines = <FILE2>;
chomp(@file2_lines);
close(FILE2);


foreach my $line1 (@file1_lines){
        my $flag='';
        my $comodin = ();

	my ($L1_chr,$L1_pos1L,$L1_pos1U,$L1_support,$L1_tefam,$L1_reads) = split('\t',$line1);
       

	foreach my $line2 (@file2_lines){
		my ($PolyA_chr,$PolyA_pos1L,$PolyA_pos1U,$PolyA_support,$PolyA_tefam,$PolyA_reads) = split('\t',$line2);
		if (($L1_chr eq $PolyA_chr) && ((abs($L1_pos1L - $PolyA_pos1L) <= 200) || (abs($L1_pos1U - $PolyA_pos1U) <= 200))){

				my $suma_support = $L1_support + $PolyA_support;
				my @coordinates = ($L1_pos1L, $L1_pos1U, $PolyA_pos1L, $PolyA_pos1U);
				my @sorted_coordinates = sort {$a <=> $b} @coordinates;

				$comodin = "$L1_chr\t$sorted_coordinates[0]\t$sorted_coordinates[3]\t$suma_support\tL1\t$L1_reads,$PolyA_reads\n";
				$flag = 'interesa';
				last;
		}

	               
	}
	if($flag eq 'interesa'){
		print ("$comodin");

	}else{
		print ("$line1\n");
	}

}

