#!/usr/bin/bash

sample=$1
tumour=$2
normal=$3
script_dir=$4
pathdir=$5
databases=$6

###### #t0
###### echo 'STARTING T0'
###### echo "perl $script_dir/scripts/cleaner_v5_2reads.pl $pathdir/$sample/$tumour/$tumour.MGE4C.TEs_ci $pathdir/$sample/$normal/totalclusters_masymenos.txt > $pathdir/$sample/cleaner5.td0.4C.out"
###### perl $script_dir/scripts/cleaner_v5_2reads.pl $pathdir/$sample/$tumour/$tumour.MGE4C.TEs_ci $pathdir/$sample/$normal/totalclusters_masymenos.txt > $pathdir/$sample/cleaner5.td0.4C.out
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td0.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td0b.4C.out"
###### perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td0.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td0b.4C.out
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td0b.4C.out $databases/db_04102015.tab > $pathdir/$sample/filter1.td0.4C.txt"
###### perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td0b.4C.out $databases/db_04102015.tab > $pathdir/$sample/filter1.td0.4C.txt
###### 
###### # add jose pancan filter here .... 
###### echo "perl $script_dir/scripts/cleaner_v5_v2.pl $pathdir/$sample/filter1.td0.4C.txt $databases/Satellites.txt > $pathdir/$sample/filter1.td0b.4C.txt"
###### perl $script_dir/scripts/cleaner_v5_v2.pl $pathdir/$sample/filter1.td0.4C.txt $databases/Satellites.txt > $pathdir/$sample/filter1.td0b.4C.txt
###### 
###### echo "perl $script_dir/scripts/TEs_poly_cleaner.pl $pathdir/$sample/filter1.td0b.4C.txt $databases/RM.tab 100 200 > $pathdir/$sample/filter2.td0.4C.txt"
###### perl $script_dir/scripts/TEs_poly_cleaner.pl $pathdir/$sample/filter1.td0b.4C.txt $databases/RM.tab 100 200 > $pathdir/$sample/filter2.td0.4C.txt
###### # substitute by Berni database of genes
###### 
###### echo "perl $script_dir/scripts/drgene2.pl $pathdir/$sample/filter2.td0.4C.txt $databases/genes.ok.tab 100 > $pathdir/$sample/filter3.td0.4C.txt"
###### perl $script_dir/scripts/drgene2.pl $pathdir/$sample/filter2.td0.4C.txt $databases/genes.ok.tab 100 > $pathdir/$sample/filter3.td0.4C.txt
###### echo 'DONE t0'
###### 
###### #t1
###### echo 'STARING t1'
###### echo "cat $pathdir/$sample/$normal/clusters_mas.td.txt $pathdir/$sample/$normal/clusters_menos.td.txt > $pathdir/$sample/$normal/totalclusters_masymenos.td.txt"
###### cat $pathdir/$sample/$normal/clusters_mas.td.txt $pathdir/$sample/$normal/clusters_menos.td.txt > $pathdir/$sample/$normal/totalclusters_masymenos.td.txt
###### 
###### echo "cat $pathdir/$sample/$normal/totalclusters_masymenos.td.txt $pathdir/$sample/$normal/totalclusters_masymenos.txt > $pathdir/$sample/$normal/totalclusters_masymenos.sum.txt"
###### cat $pathdir/$sample/$normal/totalclusters_masymenos.td.txt $pathdir/$sample/$normal/totalclusters_masymenos.txt > $pathdir/$sample/$normal/totalclusters_masymenos.sum.txt
###### 
###### echo "cat $pathdir/$sample/$tumour/$tumour.tdplus4C.TEs_ci $pathdir/$sample/$tumour/$tumour.tdminus4C.TEs_ci > $pathdir/$sample/$tumour/$tumour.tdplusminus4C.TEs_ci"
###### cat $pathdir/$sample/$tumour/$tumour.tdplus4C.TEs_ci $pathdir/$sample/$tumour/$tumour.tdminus4C.TEs_ci > $pathdir/$sample/$tumour/$tumour.tdplusminus4C.TEs_ci
###### 
###### echo "cat $pathdir/$sample/$normal/$normal.tdplus4C.TEs_ci $pathdir/$sample/$normal/$normal.tdminus4C.TEs_ci > $pathdir/$sample/$normal/$normal.tdplusminus4C.TEs_ci"
###### cat $pathdir/$sample/$normal/$normal.tdplus4C.TEs_ci $pathdir/$sample/$normal/$normal.tdminus4C.TEs_ci > $pathdir/$sample/$normal/$normal.tdplusminus4C.TEs_ci
###### 
###### echo "perl $script_dir/scripts/cleaner_v5_2reads.pl $pathdir/$sample/$tumour/$tumour.tdplusminus4C.TEs_ci $pathdir/$sample/$normal/totalclusters_masymenos.sum.txt > $pathdir/$sample/cleaner5.td1.4C.out"
###### perl $script_dir/scripts/cleaner_v5_2reads.pl $pathdir/$sample/$tumour/$tumour.tdplusminus4C.TEs_ci $pathdir/$sample/$normal/totalclusters_masymenos.sum.txt > $pathdir/$sample/cleaner5.td1.4C.out
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td1.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td1b.4C.out"
###### perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td1.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td1b.4C.out
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td1b.4C.out $databases/db_04102015.tab > $pathdir/$sample/filter1.td1.4C.txt"
###### perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td1b.4C.out $databases/db_04102015.tab > $pathdir/$sample/filter1.td1.4C.txt
###### 
###### echo "perl $script_dir/scripts/cleaner_v5_v2.pl $pathdir/$sample/filter1.td1.4C.txt $databases/Satellites.txt > $pathdir/$sample/filter1.td1b.4C.txt"
###### perl $script_dir/scripts/cleaner_v5_v2.pl $pathdir/$sample/filter1.td1.4C.txt $databases/Satellites.txt > $pathdir/$sample/filter1.td1b.4C.txt
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/filter1.td1b.4C.txt $databases/db_td_04102015.tab > $pathdir/$sample/filter2.td1.4C.txt"
###### perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/filter1.td1b.4C.txt $databases/db_td_04102015.tab > $pathdir/$sample/filter2.td1.4C.txt
###### 
###### echo "perl $script_dir/scripts/drgene2.pl $pathdir/$sample/filter2.td1.4C.txt $databases/genes.ok.tab 100 > $pathdir/$sample/filter3.td1.4C.txt"
###### perl $script_dir/scripts/drgene2.pl $pathdir/$sample/filter2.td1.4C.txt $databases/genes.ok.tab 100 > $pathdir/$sample/filter3.td1.4C.txt
###### 
###### echo "cat $pathdir/$sample/$normal/$normal.MGE4C.TEs_ci $pathdir/$sample/$normal/$normal.tdplusminus4C.TEs_ci $pathdir/$sample/$tumour/$tumour.MGE4C.TEs_ci $pathdir/$sample/$tumour/$tumour.tdplusminus4C.TEs_ci > $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt"
###### cat $pathdir/$sample/$normal/$normal.MGE4C.TEs_ci $pathdir/$sample/$normal/$normal.tdplusminus4C.TEs_ci $pathdir/$sample/$tumour/$tumour.MGE4C.TEs_ci $pathdir/$sample/$tumour/$tumour.tdplusminus4C.TEs_ci > $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt
###### echo 'DONE t1'
###### 
###### 
###### #td1 master copies;
###### echo 'STARTING td1 master copies'
###### echo "split -l 5000000 $pathdir/$sample/$tumour/$tumour.dis.sam $pathdir/$sample/$tumour/$tumour.dis.sam_"
###### split -l 5000000 $pathdir/$sample/$tumour/$tumour.dis.sam $pathdir/$sample/$tumour/$tumour.dis.sam_
###### 
###### #rm $pathdir/$sample/$tumour/$tumour.dis.sam
###### 
###### echo "perl $script_dir/scripts/matches_td_split.pl $pathdir/$sample/$tumour $pathdir/$sample/filter3.td1.4C.txt $pathdir/$sample"
###### perl $script_dir/scripts/matches_td_split.pl $pathdir/$sample/$tumour $pathdir/$sample/filter3.td1.4C.txt $pathdir/$sample
###### 
###### echo "cat $pathdir/$sample/matches_td_* | sort -k1,1 > $pathdir/$sample/matches_td1_all.ok.txt"
###### cat $pathdir/$sample/matches_td_* | sort -k1,1 > $pathdir/$sample/matches_td1_all.ok.txt
###### 
###### #rm $pathdir/$sample/matches_td_*
###### 
###### #rm $pathdir/$sample/unmatches_td_*
###### 
###### echo "perl  $script_dir/scripts/make1from2_TE_03032015.pl $pathdir/$sample/matches_td1_all.ok.txt > $pathdir/$sample/matches_td1_all.ok.1l.txt"
###### perl  $script_dir/scripts/make1from2_TE_03032015.pl $pathdir/$sample/matches_td1_all.ok.txt > $pathdir/$sample/matches_td1_all.ok.1l.txt
###### 
###### echo "perl  $script_dir/scripts/td_coordinates_v2_td2_forCGPpipeline.pl $pathdir/$sample/filter3.td1.4C.txt $pathdir/$sample/matches_td1_all.ok.1l.txt $pathdir/$sample"
###### perl  $script_dir/scripts/td_coordinates_v2_td2_forCGPpipeline.pl $pathdir/$sample/filter3.td1.4C.txt $pathdir/$sample/matches_td1_all.ok.1l.txt $pathdir/$sample
###### 
###### echo "perl  $script_dir/scripts/cleaner_v3_fortd1_v3td1_v3.pl $pathdir/$sample/filter3.td.4C.coordinates.mas.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt mas $pathdir/$sample"
###### perl  $script_dir/scripts/cleaner_v3_fortd1_v3td1_v3.pl $pathdir/$sample/filter3.td.4C.coordinates.mas.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt mas $pathdir/$sample
###### 
###### echo "perl  $script_dir/scripts/cleaner_v3_fortd1_v3td1_v3.pl $pathdir/$sample/filter3.td.4C.coordinates.menos.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt menos $pathdir/$sample"
###### perl  $script_dir/scripts/cleaner_v3_fortd1_v3td1_v3.pl $pathdir/$sample/filter3.td.4C.coordinates.menos.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt menos $pathdir/$sample
###### 
###### echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master.mas.txt > $pathdir/$sample/finaltd1.master.mas.ok.txt"
###### sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master.mas.txt > $pathdir/$sample/finaltd1.master.mas.ok.txt
###### 
###### echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.putative.mas.txt > $pathdir/$sample/finaltd1.putative.mas.ok.txt"
###### sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.putative.mas.txt > $pathdir/$sample/finaltd1.putative.mas.ok.txt
###### 
###### echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master_putative.mas.txt > $pathdir/$sample/finaltd1.master_putative.mas.ok.txt"
###### sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master_putative.mas.txt > $pathdir/$sample/finaltd1.master_putative.mas.ok.txt
###### 
###### echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master.menos.txt > $pathdir/$sample/finaltd1.master.menos.ok.txt"
###### sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master.menos.txt > $pathdir/$sample/finaltd1.master.menos.ok.txt
###### 
###### echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.putative.menos.txt > $pathdir/$sample/finaltd1.putative.menos.ok.txt"
###### sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.putative.menos.txt > $pathdir/$sample/finaltd1.putative.menos.ok.txt
###### 
###### echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master_putative.menos.txt > $pathdir/$sample/finaltd1.master_putative.menos.ok.txt"
###### sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd1.master_putative.menos.txt > $pathdir/$sample/finaltd1.master_putative.menos.ok.txt
###### 
###### echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master.mas.ok.txt > $pathdir/$sample/deftd1.master.mas.ok.txt"
###### perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master.mas.ok.txt > $pathdir/$sample/deftd1.master.mas.ok.txt
###### 
###### echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master.menos.ok.txt > $pathdir/$sample/deftd1.master.menos.ok.txt"
###### perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master.menos.ok.txt > $pathdir/$sample/deftd1.master.menos.ok.txt
###### 
###### echo "cat $pathdir/$sample/deftd1.master.mas.ok.txt $pathdir/$sample/deftd1.master.menos.ok.txt | sort -nk1,1 -nk2,2 > $pathdir/$sample/deftd1.master.masymenos.ok.txt"
###### cat $pathdir/$sample/deftd1.master.mas.ok.txt $pathdir/$sample/deftd1.master.menos.ok.txt | sort -nk1,1 -nk2,2 > $pathdir/$sample/deftd1.master.masymenos.ok.txt
###### 
###### echo "perl $script_dir/scripts/final_td1_filters.pl $pathdir/$sample/deftd1.master.masymenos.ok.txt > $pathdir/$sample/deftd1.master.ok.txt"
###### perl $script_dir/scripts/final_td1_filters.pl $pathdir/$sample/deftd1.master.masymenos.ok.txt > $pathdir/$sample/deftd1.master.ok.txt
###### 
###### echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.putative.mas.ok.txt > $pathdir/$sample/deftd1.putative.mas.ok.txt"
###### perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.putative.mas.ok.txt > $pathdir/$sample/deftd1.putative.mas.ok.txt
###### 
###### echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.putative.menos.ok.txt > $pathdir/$sample/deftd1.putative.menos.ok.txt"
###### perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.putative.menos.ok.txt > $pathdir/$sample/deftd1.putative.menos.ok.txt
###### 
###### echo "cat $pathdir/$sample/deftd1.putative.mas.ok.txt $pathdir/$sample/deftd1.putative.menos.ok.txt | sort -nk1,1 -nk2,2 > $pathdir/$sample/deftd1.putative.masymenos.ok.txt"
###### cat $pathdir/$sample/deftd1.putative.mas.ok.txt $pathdir/$sample/deftd1.putative.menos.ok.txt | sort -nk1,1 -nk2,2 > $pathdir/$sample/deftd1.putative.masymenos.ok.txt
###### 
###### echo "perl $script_dir/scripts/final_td1_filters.pl $pathdir/$sample/deftd1.putative.masymenos.ok.txt > $pathdir/$sample/deftd1.putative.ok.txt"
###### perl $script_dir/scripts/final_td1_filters.pl $pathdir/$sample/deftd1.putative.masymenos.ok.txt > $pathdir/$sample/deftd1.putative.ok.txt
###### 
###### echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master_putative.mas.ok.txt > $pathdir/$sample/deftd1.master_putative.mas.ok.txt" 
###### perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master_putative.mas.ok.txt > $pathdir/$sample/deftd1.master_putative.mas.ok.txt
###### 
###### echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master_putative.menos.ok.txt > $pathdir/$sample/deftd1.master_putative.menos.ok.txt"
###### perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd1.master_putative.menos.ok.txt > $pathdir/$sample/deftd1.master_putative.menos.ok.txt
###### 
###### echo "cat $pathdir/$sample/deftd1.master_putative.mas.ok.txt $pathdir/$sample/deftd1.master_putative.menos.ok.txt | sort -nk1,1 -nk2,2 > $pathdir/$sample/deftd1.master_putative.masymenos.ok.txt"
###### cat $pathdir/$sample/deftd1.master_putative.mas.ok.txt $pathdir/$sample/deftd1.master_putative.menos.ok.txt | sort -nk1,1 -nk2,2 > $pathdir/$sample/deftd1.master_putative.masymenos.ok.txt
###### 
###### echo "perl $script_dir/scripts/final_td1_filters.pl $pathdir/$sample/deftd1.master_putative.masymenos.ok.txt > $pathdir/$sample/deftd1.master_putative.ok.txt"
###### perl $script_dir/scripts/final_td1_filters.pl $pathdir/$sample/deftd1.master_putative.masymenos.ok.txt > $pathdir/$sample/deftd1.master_putative.ok.txt
###### echo 'DONE td1 master copies'
###### 
###### 
###### #t2:
###### echo 'STARTING t2'
###### echo "perl $script_dir/scripts/cleaner_v5_2reads.pl $pathdir/$sample/$tumour/$tumour.chr2chr4C.TEs_ci $pathdir/$sample/$normal/totalclusters_masymenos.td.txt > $pathdir/$sample/cleaner5.td2.4C.out"
###### perl $script_dir/scripts/cleaner_v5_2reads.pl $pathdir/$sample/$tumour/$tumour.chr2chr4C.TEs_ci $pathdir/$sample/$normal/totalclusters_masymenos.td.txt > $pathdir/$sample/cleaner5.td2.4C.out
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td2.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td2b.4C.out"
###### perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td2.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td2b.4C.out
###### 
###### echo "perl $script_dir/scripts/cleaner_v5.pl $pathdir/$sample/cleaner5.td2.4C.out $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt > $pathdir/$sample/cleaner5.td2b.4C.out"
###### perl $script_dir/scripts/cleaner_v5_v2.pl $pathdir/$sample/cleaner5.td2b.4C.out $databases/Satellites.txt > $pathdir/$sample/cleaner5b.td2.4C.out
###### 
###### echo "perl $script_dir/scripts/drgene2.pl $pathdir/$sample/cleaner5b.td2.4C.out $databases/genes.ok.tab 100 > $pathdir/$sample/filter3.td2.4C.txt"
###### perl $script_dir/scripts/drgene2.pl $pathdir/$sample/cleaner5b.td2.4C.out $databases/genes.ok.tab 100 > $pathdir/$sample/filter3.td2.4C.txt
###### 
###### #rm $pathdir/$sample/$normal/clusters.PolyA.masymenos.txt
###### echo 'DONE t2'
###### 
###### 
###### #td2 master copies;
###### echo 'STARTING td2 master copies'
###### echo "perl $script_dir/scripts/matches_td_split.pl $pathdir/$sample/$tumour $pathdir/$sample/filter3.td2.4C.txt $pathdir/$sample"
###### perl $script_dir/scripts/matches_td_split.pl $pathdir/$sample/$tumour $pathdir/$sample/filter3.td2.4C.txt $pathdir/$sample
###### 
###### echo "cat $pathdir/$sample/matches_td_* | sort -k1,1 > $pathdir/$sample/matches_td2_all.ok.txt"
###### cat $pathdir/$sample/matches_td_* | sort -k1,1 > $pathdir/$sample/matches_td2_all.ok.txt
###### 
###### #rm $pathdir/$sample/$tumour/$tumour.dis.sam_*
###### 
###### #rm $pathdir/$sample/matches_td_*
###### 
###### #rm $pathdir/$sample/unmatches_td_*
###### 
###### echo "perl $script_dir/scripts/make1from2_TE_03032015.pl $pathdir/$sample/matches_td2_all.ok.txt > $pathdir/$sample/matches_td2_all.ok.1l.txt"
###### perl $script_dir/scripts/make1from2_TE_03032015.pl $pathdir/$sample/matches_td2_all.ok.txt > $pathdir/$sample/matches_td2_all.ok.1l.txt
###### 
###### echo "perl $script_dir/scripts/td_coordinates_v2_td2_forCGPpipeline.pl $pathdir/$sample/filter3.td2.4C.txt $pathdir/$sample/matches_td2_all.ok.1l.txt $pathdir/$sample"
###### perl $script_dir/scripts/td_coordinates_v2_td2_forCGPpipeline.pl $pathdir/$sample/filter3.td2.4C.txt $pathdir/$sample/matches_td2_all.ok.1l.txt $pathdir/$sample

echo "perl $script_dir/scripts/cleaner_v3_fortd1_v5.pl $pathdir/$sample/filter3.td.4C.coordinates.mas.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt mas $pathdir/$sample"
perl $script_dir/scripts/cleaner_v3_fortd1_v5.pl $pathdir/$sample/filter3.td.4C.coordinates.mas.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt mas $pathdir/$sample

echo "perl $script_dir/scripts/cleaner_v3_fortd1_v5.pl $pathdir/$sample/filter3.td.4C.coordinates.menos.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt menos $pathdir/$sample"
perl $script_dir/scripts/cleaner_v3_fortd1_v5.pl $pathdir/$sample/filter3.td.4C.coordinates.menos.txt $databases/master_db.txt $pathdir/$sample/$sample.allMGE4Candtdplusminus.txt $databases/master.txt menos $pathdir/$sample

echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master.mas.txt > $pathdir/$sample/finaltd2.master.mas.ok.txt"
sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master.mas.txt > $pathdir/$sample/finaltd2.master.mas.ok.txt

echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.putative.mas.txt > $pathdir/$sample/finaltd2.putative.mas.ok.txt"
sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.putative.mas.txt > $pathdir/$sample/finaltd2.putative.mas.ok.txt

echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master_putative.mas.txt > $pathdir/$sample/finaltd2.master_putative.mas.ok.txt"
sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master_putative.mas.txt > $pathdir/$sample/finaltd2.master_putative.mas.ok.txt

echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master.menos.txt > $pathdir/$sample/finaltd2.master.menos.ok.txt"
sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master.menos.txt > $pathdir/$sample/finaltd2.master.menos.ok.txt

echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.putative.menos.txt > $pathdir/$sample/finaltd2.putative.menos.ok.txt"
sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.putative.menos.txt > $pathdir/$sample/finaltd2.putative.menos.ok.txt

echo "sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master_putative.menos.txt > $pathdir/$sample/finaltd2.master_putative.menos.ok.txt"
sort -nk1,1 -nk2,2 -nk14,14 -nk15,15 $pathdir/$sample/finaltd2.master_putative.menos.txt > $pathdir/$sample/finaltd2.master_putative.menos.ok.txt

echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master.mas.ok.txt > $pathdir/$sample/deftd2.master.mas.ok.txt"
perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master.mas.ok.txt > $pathdir/$sample/deftd2.master.mas.ok.txt

echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master.menos.ok.txt > $pathdir/$sample/deftd2.master.menos.ok.txt"
perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master.menos.ok.txt > $pathdir/$sample/deftd2.master.menos.ok.txt

echo "perl $script_dir/scripts/final_td2_filters.pl $pathdir/$sample/deftd2.master.mas.ok.txt $pathdir/$sample/deftd2.master.menos.ok.txt > $pathdir/$sample/deftd2.master.ok.txt"
perl $script_dir/scripts/final_td2_filters.pl $pathdir/$sample/deftd2.master.mas.ok.txt $pathdir/$sample/deftd2.master.menos.ok.txt > $pathdir/$sample/deftd2.master.ok.txt

echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.putative.mas.ok.txt > $pathdir/$sample/deftd2.putative.mas.ok.txt"
perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.putative.mas.ok.txt > $pathdir/$sample/deftd2.putative.mas.ok.txt

echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.putative.menos.ok.txt > $pathdir/$sample/deftd2.putative.menos.ok.txt"
perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.putative.menos.ok.txt > $pathdir/$sample/deftd2.putative.menos.ok.txt

echo "perl $script_dir/scripts/final_td2_filters.pl $pathdir/$sample/deftd2.putative.mas.ok.txt $pathdir/$sample/deftd2.putative.menos.ok.txt > $pathdir/$sample/deftd2.putative.ok.txt"
perl $script_dir/scripts/final_td2_filters.pl $pathdir/$sample/deftd2.putative.mas.ok.txt $pathdir/$sample/deftd2.putative.menos.ok.txt > $pathdir/$sample/deftd2.putative.ok.txt

echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master_putative.mas.ok.txt > $pathdir/$sample/deftd2.master_putative.mas.ok.txt"
perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master_putative.mas.ok.txt > $pathdir/$sample/deftd2.master_putative.mas.ok.txt

echo "perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master_putative.menos.ok.txt > $pathdir/$sample/deftd2.master_putative.menos.ok.txt"
perl $script_dir/scripts/nuevo_script_td2.pl $pathdir/$sample/finaltd2.master_putative.menos.ok.txt > $pathdir/$sample/deftd2.master_putative.menos.ok.txt

echo "perl $script_dir/scripts/final_td2_filters.pl $pathdir/$sample/deftd2.master_putative.mas.ok.txt $pathdir/$sample/deftd2.master_putative.menos.ok.txt > $pathdir/$sample/deftd2.master_putative.ok.txt"
perl $script_dir/scripts/final_td2_filters.pl $pathdir/$sample/deftd2.master_putative.mas.ok.txt $pathdir/$sample/deftd2.master_putative.menos.ok.txt > $pathdir/$sample/deftd2.master_putative.ok.txt

echo "perl $script_dir/scripts/111111111.pl $pathdir/$sample $sample > $pathdir/$sample/$sample.1111.txt"
perl $script_dir/scripts/111111111.pl $pathdir/$sample $sample > $pathdir/$sample/$sample.1111.txt

echo "perl $script_dir/scripts/111111111.pl $pathdir/$sample $sample > $pathdir/$sample/$sample.1111.txt"
perl $script_dir/scripts/111111111_v2.4.pl $pathdir/$sample $sample
echo 'DONE td2 master copies'

sleep 10