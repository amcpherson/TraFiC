#!/usr/bin/perl -w
#use strict;

#este program genera una sola linea a partir de una linea y la anterior si el nombre de los reads es identico;

if (scalar(@ARGV)!=1){
	print "ERROR: there must be one input file\n";
	exit -1
}

my $infile = $ARGV[0];

open (FILE,"<$infile");

my $preline = "";
my $nline = 0;
	
	while ($line = <FILE>) {
		chomp $line;

		if ($nline == 1){
		
			print("$line\n");

		}
		
		if ($nline > 0) {

			my @array_prev = split('\t', $preline);
			my $prev_id = $array_prev[0];

			my @array_line = split('\t', $line);
			my $line_id = $array_line[0];

			if ($prev_id ne $line_id){
				print("$line\t$line\n");
			}

			
		}
		
		$nline ++;
		$preline = $line;
	}
close FILE;

	
