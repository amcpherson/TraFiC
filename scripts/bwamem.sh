#!/usr/bin/bash

sample=$1
tumour=$2
script_dir=$3
pathdir=$4
bwamem_path=$5
bwa_threads=$6
databases=$7

echo "$bwamem_path/bwa mem -t $bwa_threads $databases/sequences.fa $pathdir/$sample/$tumour/$tumour.plusandminus_reads.tr.fa  > $pathdir/$sample/$tumour/bwa.TEs.txt"
$bwamem_path/bwa mem -t $bwa_threads $databases/sequences.fa $pathdir/$sample/$tumour/$tumour.plusandminus_reads.tr.fa > $pathdir/$sample/$tumour/bwa.TEs.txt 

echo "$bwamem_path/bwa mem -t $bwa_threads $databases/PolyA.fa $pathdir/$sample/$tumour/$tumour.plusandminus_reads.tr.fa  > $pathdir/$sample/$tumour/bwa.PolyA.txt"
$bwamem_path/bwa mem -t $bwa_threads $databases/PolyA.fa $pathdir/$sample/$tumour/$tumour.plusandminus_reads.tr.fa > $pathdir/$sample/$tumour/bwa.PolyA.txt

#rm $pathdir/$sample/$tumour/$tumour.plusandminus_reads.tr.fa # DON´T DELETE THIS FILE !!!!

#-------------------------------------
#sed 's/\//\t/g' $pathdir/$sample/$tumour/bwa.TEs.txt | sed 's/#/\t/g' | awk 'OFS = "\t" {if ($1 !~ "@"){print"100",$1,$5,$4,$3}}' | awk 'OFS = "\t" {if (($4 == "LINE") || ($4 == "SINE") || ($4 == "Other") || ($4 == "LTR")){print$0}}' > $pathdir/$sample/$tumour/masked3_masymenos.txt
perl -e 'while(<>){ chomp($_); $_ =~ /(.*?)\t(.*?)\t(.*?)\t(.*?)\t(.*?)\t/; $one=$1;$two=$2;$three=$3;$four=$4;$five=$5;$three=~ s/[#\/]/\t/g; print"$one\t$two\t$three\t$four\t$five\n";}' $pathdir/$sample/$tumour/bwa.TEs.txt | awk 'OFS = "\t" {if ($1 !~ "@"){print"100",$1,$5,$4,$3}}' | awk 'OFS = "\t" {if (($4 == "LINE") || ($4 == "SINE") || ($4 == "Other") || ($4 == "LTR")){print$0}}' > $pathdir/$sample/$tumour/masked3_masymenos.txt


#sed 's/\//\t/g' $pathdir/$sample/$tumour/bwa.PolyA.txt | sed 's/#/\t/g' | awk 'OFS = "\t" {if ($1 !~ "@"){print"100",$1,$5,$4,$3}}' | awk 'OFS = "\t" {if ($4 == "PolyA"){print$0}}' > $pathdir/$sample/$tumour/masked3_masymenos.PolyA.txt
perl -e 'while(<>){ chomp($_); $_ =~ /(.*?)\t(.*?)\t(.*?)\t(.*?)\t(.*?)\t/; $one=$1;$two=$2;$three=$3;$four=$4;$five=$5;$three=~ s/[#\/]/\t/g; print"$one\t$two\t$three\t$four\t$five\n";}' $pathdir/$sample/$tumour/bwa.PolyA.txt | awk 'OFS = "\t" {if ($1 !~ "@"){print"100",$1,$5,$4,$3}}' | awk 'OFS = "\t" {if ($4 == "PolyA"){print$0}}' > $pathdir/$sample/$tumour/masked3_masymenos.PolyA.txt

#-------------------------------------

#rm $pathdir/$sample/$tumour/bwa.PolyA.txt
#rm $pathdir/$sample/$tumour/bwa.TEs.txt

cat $pathdir/$sample/$tumour/$tumour.plus_reads.txt $pathdir/$sample/$tumour/$tumour.minus_reads.txt > $pathdir/$sample/$tumour/totalread_masymenos.txt

#rm $pathdir/$sample/$tumour/$tumour.plus_reads.txt
#rm $pathdir/$sample/$tumour/$tumour.minus_reads.txt

awk '{if ($3 == "Alu"){print$0}}' $pathdir/$sample/$tumour/masked3_masymenos.txt > $pathdir/$sample/$tumour/masked3_masymenos_Alu.txt

awk '{if ($3 != "Alu"){print$0}}' $pathdir/$sample/$tumour/masked3_masymenos.txt > $pathdir/$sample/$tumour/masked3_masymenos_nonAlu.txt

#rm $pathdir/$sample/$tumour/masked3_masymenos.txt

cat $pathdir/$sample/$tumour/$tumour.chrpos.sam $pathdir/$sample/$tumour/$tumour.chrneg.sam > $pathdir/$sample/$tumour/totalread_chr_masymenos.txt

#rm $pathdir/$sample/$tumour/$tumour.chrpos.sam
#rm $pathdir/$sample/$tumour/$tumour.chrneg.sam

perl $script_dir/scripts/copy_id_finder-3_TE.pl $pathdir/$sample/$tumour/masked3_masymenos_nonAlu.txt $pathdir/$sample/$tumour/totalread_masymenos.txt > $pathdir/$sample/$tumour/masked4_masymenos_nonAlu.txt

perl $script_dir/scripts/copy_id_finder-3_TE.pl $pathdir/$sample/$tumour/masked3_masymenos_Alu.txt $pathdir/$sample/$tumour/totalread_chr_masymenos.txt > $pathdir/$sample/$tumour/masked4_masymenos_Alu.txt

#rm $pathdir/$sample/$tumour/totalread_chr_masymenos.txt

#rm $pathdir/$sample/$tumour/masked3_masymenos_nonAlu.txt
#rm $pathdir/$sample/$tumour/masked3_masymenos_Alu.txt

perl $script_dir/scripts/copy_id_finder-3_TE.pl $pathdir/$sample/$tumour/masked3_masymenos.PolyA.txt $pathdir/$sample/$tumour/totalread_masymenos.txt > $pathdir/$sample/$tumour/masked4_masymenos.PolyA.txt

#rm $pathdir/$sample/$tumour/masked3_masymenos.PolyA.txt
#rm $pathdir/$sample/$tumour/totalread_masymenos.txt

cat $pathdir/$sample/$tumour/masked4_masymenos_nonAlu.txt $pathdir/$sample/$tumour/masked4_masymenos_Alu.txt > $pathdir/$sample/$tumour/masked4_masymenos.txt

#rm $pathdir/$sample/$tumour/masked4_masymenos_nonAlu.txt
#rm $pathdir/$sample/$tumour/masked4_masymenos_Alu.txt

awk 'OFS="\t" {print$6,$7,$8,$3,$10}' $pathdir/$sample/$tumour/masked4_masymenos.txt > $pathdir/$sample/$tumour/masked5_masymenos.txt
awk 'OFS="\t" {print$6,$7,$8,$3,$10}' $pathdir/$sample/$tumour/masked4_masymenos.PolyA.txt > $pathdir/$sample/$tumour/masked5_masymenos.PolyA.txt

#rm $pathdir/$sample/$tumour/masked4_masymenos.txt rm $pathdir/$sample/$tumour/masked4_masymenos.PolyA.txt

awk '{if ($5 == "+"){print$0}}' $pathdir/$sample/$tumour/masked5_masymenos.txt > $pathdir/$sample/$tumour/MASKEDreads_mas.txt
awk '{if ($5 == "+"){print$0}}' $pathdir/$sample/$tumour/masked5_masymenos.PolyA.txt > $pathdir/$sample/$tumour/MASKEDreads_mas.PolyA.txt

awk '{if ($5 == "-"){print$0}}' $pathdir/$sample/$tumour/masked5_masymenos.txt > $pathdir/$sample/$tumour/MASKEDreads_menos.txt
awk '{if ($5 == "-"){print$0}}' $pathdir/$sample/$tumour/masked5_masymenos.PolyA.txt > $pathdir/$sample/$tumour/MASKEDreads_menos.PolyA.txt

#rm $pathdir/$sample/$tumour/masked5_masymenos.txt
#rm $pathdir/$sample/$tumour/masked5_masymenos.PolyA.txt

sort -k 2,2 -k 3,3n $pathdir/$sample/$tumour/MASKEDreads_mas.txt > $pathdir/$sample/$tumour/MASKEDreads_mas.ok.txt
sort -k 2,2 -k 3,3n $pathdir/$sample/$tumour/MASKEDreads_mas.PolyA.txt > $pathdir/$sample/$tumour/MASKEDreads_mas.ok.PolyA.txt

sort -k 2,2 -k 3,3n $pathdir/$sample/$tumour/MASKEDreads_menos.txt > $pathdir/$sample/$tumour/MASKEDreads_menos.ok.txt
sort -k 2,2 -k 3,3n $pathdir/$sample/$tumour/MASKEDreads_menos.PolyA.txt > $pathdir/$sample/$tumour/MASKEDreads_menos.ok.PolyA.txt

#rm $pathdir/$sample/$tumour/MASKEDreads_mas.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_menos.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_mas.PolyA.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_menos.PolyA.txt