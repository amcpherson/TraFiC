#!/usr/bin/bash

sample=$1
normal=$2
script_dir=$3
pathdir=$4

#sigo adelante con los TEs;

cat $pathdir/$sample/$normal/clusters_mas.txt $pathdir/$sample/$normal/clusters_menos.txt > $pathdir/$sample/$normal/totalclusters_masymenos.txt


awk 'OFS="\t" {if (($4 >= 4) && ($1 ne "MT") && ($1 ne "Y") && ($1 !~ /hs/) && ($1 !~ /GL/)) {print$0}}' $pathdir/$sample/$normal/clusters_mas.txt > $pathdir/$sample/$normal/clusters_mas.4C.txt

awk 'OFS="\t" {if (($4 >= 4) && ($1 ne "MT") && ($1 ne "Y") && ($1 !~ /hs/) && ($1 !~ /GL/)) {print$0}}' $pathdir/$sample/$normal/clusters_menos.txt > $pathdir/$sample/$normal/clusters_menos.4C.txt

perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$normal/clusters_mas.4C.txt $pathdir/$sample/$normal/clusters_menos.4C.txt $pathdir/$sample/$normal $normal.MGE4C

