#!/usr/bin/perl -w

#igual que copy_id_finder-2_TE.pl, pero para transductions;
#igual que /nfs/team78pc8/jt14/TEs/matches_td.pl pero echo para el split del input ($tumour.dis.sam esta splitado);

use strict;

if (scalar(@ARGV) != 3){
	print "Incorrect number of files\n";
}

my $pathdir_tumour = $ARGV[0];
my $infile2 = $ARGV[1];
my $pathdir = $ARGV[2];

my @infiles = glob "$pathdir_tumour/*.dis.sam_*";

foreach (@infiles){

my @array = split('_', $_);
my $coletilla = $array[-1];

my $outfile1 = "$pathdir/matches_td_$coletilla.txt";
my $outfile2 = "$pathdir/unmatches_td_$coletilla.txt";
my %hash = ();


open(FILE1, "<$_") or die("Couldn't open $_: $!\n");


while (my $line1 = <FILE1>){
	$line1 =~ s/\n//g;
	my @aline1 = split('\t', $line1);
	my $file1_readname = $aline1[0];
#	print "$file1_readname-";
	$hash{$file1_readname} = $line1;
}


open(FILE2, "<$infile2") or die("Couldn't open $infile2: $!\n");



open(OUT1, ">$outfile1") or die("Couldn't open $outfile1");
open(OUT2, ">$outfile2") or die("Couldn't open $outfile2");

	while (my $line2 = <FILE2>){ #reading file2 line by line
		$line2 =~ s/\n//g; #removing enters
		my @aline2 = split('\t', $line2); #fields of each line are stored in an array
		my @reads_pos = split(',', $aline2[5]);
		my @reads_neg = split(',', $aline2[11]);
		my @merge = (@reads_pos,@reads_neg);
		foreach (@merge){
	#		my $file2_readname = $aline2[0]; #this variable contains the readname of each line in file2
	#	print "$file2_readname-";
			if ($hash{$_}){
				print(OUT1 "$hash{$_}\t$line2\n");
			}else{
				print(OUT2 "$line2\n");
			}
		}
	}
}
close(OUT1);
close(OUT2);
close(FILE1);
close(FILE2);
