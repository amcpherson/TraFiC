#This script compares the files deftd2.ok.mas y deftd2.ok.menos and filter false td2 predictions;

#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use Utils;


if (scalar(@ARGV) != 2){
	print "Incorrect number of inputs";
}

my $infile1 = $ARGV[0];#contiene deftd2.ok.mas
my $infile2 = $ARGV[1];#contiene deftd2.ok.menos

open(FILE1, "<$infile1") or die ("Couldn't open $infile1: $!\n");
my @file1_lines = <FILE1>;
close(FILE1);

open(FILE2, "<$infile2") or die ("Couldn't open $infile2: $!\n");
my @file2_lines = <FILE2>;
close(FILE2);

my $chr2data = slice_by_chr(\@file2_lines);

my @positions = ();

foreach my $line1 (@file1_lines){
	chomp $line1;
	my ($chr_hit_pos,$pos1_hit_pos,$pos2_hit_pos,$gene,$chr_master_pos,$pos1_master_pos,$pos2_master_pos,$master_condition,$chr_cluster_pos,$pos1_cluster_pos,$pos2_cluster_pos,$reads_pos) = split('\t',$line1);
	
	if (not exists $chr2data->{$chr_hit_pos})
	{
		next;
	}
	
	my @chr_data = @{$chr2data->{$chr_hit_pos}};
		
	foreach my $line2 (@chr_data){
		chomp $line2;
		my ($chr_hit_neg,$pos1_hit_neg,$pos2_hit_neg,$gene_neg,$chr_master_neg,$pos1_master_neg,$pos2_master_neg,$master_condition_neg,$chr_cluster_neg,$pos1_cluster_neg,$pos2_cluster_neg,$reads_neg) = split('\t',$line2);
			
		if (($chr_hit_pos ne "MT") && ($chr_hit_pos ne "Y") && ($chr_hit_pos eq $chr_hit_neg) && ($pos1_hit_pos == $pos1_hit_neg) && ($pos2_hit_pos == $pos2_hit_neg) && ($chr_master_pos eq $chr_master_neg) && (abs($pos1_master_pos - $pos1_master_neg) < 10000) && (abs($pos1_master_pos - $pos2_master_neg) < 10000)){
		
			push (@positions, $pos1_cluster_pos, $pos2_cluster_pos, $pos1_cluster_neg, $pos2_cluster_neg);
			my @sorted_pos = sort {$a <=> $b} @positions;
			my $suma_reads = ($reads_pos + $reads_neg);

			if (($master_condition eq "plus") && ($sorted_pos[-1] > $pos1_master_pos)){

				print "$chr_hit_pos\t$pos1_hit_pos\t$pos2_hit_pos\t$gene\t$chr_master_pos\t$pos1_master_pos\t$pos2_master_pos\t$master_condition\t$chr_cluster_pos\t$sorted_pos[0]\t$sorted_pos[-1]\t$suma_reads\n";

				@positions = ();

			}

			if (($master_condition eq "minus") && ($pos2_master_pos > $sorted_pos[0])){

				print "$chr_hit_pos\t$pos1_hit_pos\t$pos2_hit_pos\t$gene\t$chr_master_pos\t$pos1_master_pos\t$pos2_master_pos\t$master_condition\t$chr_cluster_pos\t$sorted_pos[0]\t$sorted_pos[-1]\t$suma_reads\n";

				@positions = ();

			}

			if ($master_condition eq "putative"){

				print "$chr_hit_pos\t$pos1_hit_pos\t$pos2_hit_pos\t$gene\t$chr_master_pos\t$pos1_master_pos\t$pos2_master_pos\t$master_condition\t$chr_cluster_pos\t$sorted_pos[0]\t$sorted_pos[-1]\t$suma_reads\n";

				@positions = ();

			}

		}
	}
}

close(FILE1);
close(FILE2);
