#!/usr/bin/bash

sample=$1
tumour=$2
script_dir=$3
pathdir=$4


#CLUSTERING FOR MGE

awk '{if (($4 >= 4) && ($1 ne "MT") && ($1 ne "Y") && ($1 !~ /hs/) && ($1 !~ /GL/)) {print$0}}' $pathdir/$sample/$tumour/clusters_mas.txt > $pathdir/$sample/$tumour/clusters_mas.4C.txt

awk '{if (($4 >= 4) && ($1 ne "MT") && ($1 ne "Y") && ($1 !~ /hs/) && ($1 !~ /GL/)){print$0}}' $pathdir/$sample/$tumour/clusters_menos.txt > $pathdir/$sample/$tumour/clusters_menos.4C.txt

perl $script_dir/scripts/script005_v3TEs.pl $pathdir/$sample/$tumour/clusters_mas.4C.txt $pathdir/$sample/$tumour/clusters_menos.4C.txt $pathdir/$sample/$tumour $tumour.MGE4C

