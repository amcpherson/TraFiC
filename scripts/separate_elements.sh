#!/usr/bin/bash

sample=$1
tumour=$2
pathdir=$3


#separar por tipo de elemento

awk 'OFS="\t" {if ($4 == "Alu"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_mas.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_Alu_mas.ok.txt
awk 'OFS="\t" {if ($4 == "L1"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_mas.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_L1_mas.ok.txt
awk 'OFS="\t" {if ($4 == "Other"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_mas.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_SVA_mas.ok.txt
awk 'OFS="\t" {if ($4 == "ERVK"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_mas.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_ERVK_mas.ok.txt

awk 'OFS="\t" {if ($4 == "Alu"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_menos.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_Alu_menos.ok.txt
awk 'OFS="\t" {if ($4 == "L1"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_menos.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_L1_menos.ok.txt
awk 'OFS="\t" {if ($4 == "Other"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_menos.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_SVA_menos.ok.txt
awk 'OFS="\t" {if ($4 == "ERVK"){print$0}}' $pathdir/$sample/$tumour/MASKEDreads_menos.ok.txt > $pathdir/$sample/$tumour/MASKEDreads_ERVK_menos.ok.txt

#rm $pathdir/$sample/$tumour/MASKEDreads_mas.ok.txt
#rm $pathdir/$sample/$tumour/MASKEDreads_menos.ok.txt

